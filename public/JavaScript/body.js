console.log("JavaScript in body is OK.");

// Webページ内に「class="prism"」の指定されたpre要素が存在すれば
if ( document.querySelector('pre.prism') ) {
    // prism.cssファイルを動的に読み込む
    var pmcss = document.createElement('link');
    pmcss.rel = 'stylesheet';
    pmcss.href = 'https://minorutazakura.gitlab.io/personal-website/CSS/prism.css';
    document.head.appendChild(pmcss);
    // prism.jsファイルを動的に読み込む
    var pmjs = document.createElement("script");
    pmjs.src = "https://minorutazakura.gitlab.io/personal-website/JavaScript/prism.js";
    document.head.appendChild(pmjs);
}

// Webページ内に「class="mathjax"」の指定された要素が存在すれば
if ( document.querySelector('.mathjax') ) {
    // PolyfillのスクリプトをCDN経由で読む
    let polyfilljs = document.createElement("script");
    polyfilljs.src = "https://polyfill.io/v3/polyfill.min.js?features=es6";
    document.head.appendChild(polyfilljs);
    // MathJaxのスクリプトをCDN経由で読み込む
    let mathjaxjs = document.createElement("script");
    mathjaxjs.src = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js";
    mathjaxjs.async = true;
    mathjaxjs.setAttribute("id","MathJax-Script");
    document.head.appendChild(mathjaxjs);
}